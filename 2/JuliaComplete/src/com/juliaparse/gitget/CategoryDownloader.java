package com.juliaparse.gitget;

import java.io.IOException;
import java.util.Iterator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

// This initializes the lookups for categories on grammar usage for better analysis

public class CategoryDownloader {
	
	@SuppressWarnings("static-access")
    public static void main(String[] args) throws InterruptedException, IOException {
            String lastElement;
            String currentElement;
            Elements sections, secName, fcts;
            String nextPage = "";
            Document doc;

            // Pull all the HTML from the Julia Document
            doc = Jsoup.connect("http://docs.julialang.org/en/latest/stdlib/base/index.html").userAgent("Mozilla").get();
            
            // get all headers for types
            sections = doc.select("div.body > div.section");
            
            for(Iterator<Element> i = sections.iterator(); i.hasNext(); ) {
                Element item = i.next();
                
                secName = item.select("h1");
                String secNameTxt = secName.get(0).text().substring(0, secName.get(0).text().length()-1);
                System.out.println("SECTION NAME:"+secNameTxt);
                System.out.print("FCTS:");
                
                fcts = item.select("dl.function > dt > tt.descname");
                for(Iterator<Element> j = fcts.iterator(); j.hasNext(); ) {
                	Element itemJ = j.next();
                	System.out.print(itemJ.text()+", ");
                	Config.gramCatLookup.put(itemJ.text(), secNameTxt);
                }
                System.out.println("");
            }
            
    }

}
