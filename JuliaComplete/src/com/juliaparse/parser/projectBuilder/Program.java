package com.juliaparse.parser.projectBuilder;

import java.util.ArrayList;


public class Program {
	
	String programName;
	Integer LOC = 0;
	static int totalLOC = 0;
	ArrayList<Grammar> grammar = new ArrayList<Grammar>();

	public Program(String programName, Integer lOC, ArrayList<Grammar> arrayList) {
		this.programName = programName;
		this.LOC += lOC;
		this.grammar.addAll(arrayList);
		totalLOC += LOC;
	}

	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}

	public Integer getLOC() {
		return LOC;
	}

	public void setLOC(Integer lOC) {
		LOC = lOC;
	}

	public int getTotalLOC() {
		return totalLOC;
	}
	
	@Override
	public String toString() {
		
		return programName;
	}

	public ArrayList<Grammar> getGrammar() {
		return grammar;
	}

	public void setGrammar(ArrayList<Grammar> grammar) {
		this.grammar = grammar;
	}
	

	
}
