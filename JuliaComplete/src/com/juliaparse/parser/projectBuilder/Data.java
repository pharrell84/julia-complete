package com.juliaparse.parser.projectBuilder;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Data {

	private static Data instance = null;
	private ArrayList<Grammar> grammar = new ArrayList<Grammar>();

	private Data() {
		// Enforce Singleton
	}

	public static Data getInstance() {

		if (instance == null) {
			instance = new Data();
		}

		return instance;
	}

	public ArrayList<Grammar> loadData() throws FileNotFoundException, IOException, ParseException {
		grammar.clear();
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new FileReader(Configurations.PATH_TO_JSON_FILE));
		JSONObject jsonObject = (JSONObject) obj;
		JSONArray rules = (JSONArray) jsonObject.get("grammar");

		for (int i = 0; i < rules.size(); i++) {
			JSONObject object = (JSONObject) rules.get(i);
			grammar.add(new Grammar(object.get("category").toString(), object.get("subCategory").toString(), object
					.get("name").toString()));

			// System.out.println(object.toString());
		}
		// System.out.println(grammer);

		return grammar;
	}

	public ArrayList<Grammar> getGrammar() throws FileNotFoundException, IOException, ParseException {
		if (grammar.size() < 0)
			loadData();

		return grammar;
	}

}
