package com.juliaparse.parser.projectBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class GQM {
	private ArrayList<Program> programs = new ArrayList<Program>();
	private Map<String, Integer> parallelUsage = new HashMap<String, Integer>();
	private Map<String, Integer> languageCore = new HashMap<String, Integer>();


	private double mean;
	private double median;
	private double mode;

	public GQM(ArrayList<Program> programs) {

		this.programs.addAll(programs);
	}

	// These methods can help us solve our GQMs

	// How large are Julia programs?
	public void juliaProgramsSize() {

		ArrayList<Integer> programLOC = new ArrayList<Integer>();
		for (Program program : programs) {
			programLOC.add(program.getLOC());
		}

		// Mean
		double sum = 0;
		for (Program program : programs) {
			sum += program.getLOC();
		}
		this.mean = sum / programs.size();

		// Median
		int middle = programLOC.size() / 2;
		if (programLOC.size() % 2 == 1) {
			this.median = programLOC.get(middle);
		} else {
			this.median = programLOC.get(middle - 1) + programLOC.get(middle) / 2.0;
		}

		// Mode
		int maxValue = 0, maxCount = 0;
		for (int i = 0; i < programLOC.size(); ++i) {
			int count = 0;
			for (int j = 0; j < programLOC.size(); ++j) {
				if (programLOC.get(j) == programLOC.get(i))
					++count;
			}
			if (count > maxCount) {
				maxCount = count;
				maxValue = programLOC.get(i);
			}
		}
		this.mode = maxValue;

	}

	// which features define the core?
	@SuppressWarnings("unchecked")
	public void languageCore() {

		Map<String, Integer> languageTemp = new HashMap<String, Integer>();
		//Show total usage of each grammar item?
		for (Program progam : programs) {
			for (Grammar grammar : progam.getGrammar()) {

				if(languageTemp.containsKey(grammar.getName())){
					languageTemp.put(grammar.getName(), languageTemp.get(grammar.getName()) + grammar.getUsed());
				}
				else{
					languageTemp.put(grammar.getName(), grammar.getUsed());
				}
				
			}
		}
		
		//Sort by values for printing out
		Map<String, Integer> languageCore = sortByComparator(languageTemp);
		this.languageCore = languageCore;
		System.out.println(this.languageCore);
		
	}
	
	@SuppressWarnings("unchecked")
	private static Map sortByComparator(Map unsortMap) {
		 
		List list = new LinkedList(unsortMap.entrySet());
 
		// sort list based on comparator
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) (o2)).getValue())
                                       .compareTo(((Map.Entry) (o1)).getValue());
			}
		});
 
		// put sorted list into map again
                //LinkedHashMap make sure order in which keys were inserted
		Map sortedMap = new LinkedHashMap();
		for (Iterator it = list.iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}

	// parallel features of the language used
	public void parallelFeatures() {
		ArrayList<String> parallelFeatures = new ArrayList<String>();
		parallelFeatures.add("addprocs");
		parallelFeatures.add("nprocs");
		parallelFeatures.add("nworkers");
		parallelFeatures.add("procs");
		parallelFeatures.add("workers");
		parallelFeatures.add("rmprocs");//
		parallelFeatures.add("interrupt");
		parallelFeatures.add("myid");
		parallelFeatures.add("pmap");
		parallelFeatures.add("remotecall");
		parallelFeatures.add("wait");
		parallelFeatures.add("fetch");
		parallelFeatures.add("remotecall_wait");
		parallelFeatures.add("remotecall_fetch");
		parallelFeatures.add("put");
		parallelFeatures.add("take");
		parallelFeatures.add("isready"); //
		parallelFeatures.add("RemoteRef");
		parallelFeatures.add("timedwait"); //
		parallelFeatures.add("@spawn");
		parallelFeatures.add("@spawnat");
		parallelFeatures.add("@fetch");
		parallelFeatures.add("@fetchfrom"); //
		parallelFeatures.add("@async");
		parallelFeatures.add("@sync");
		// These are the paralle features of julia

		// We could create a list of the programs that prints out rows with the number of parallel features used.
		for (Program progam : programs) {
			parallelUsage.put(progam.getProgramName(), 0);
			for (Grammar grammar : progam.getGrammar()) {
				if (parallelFeatures.contains(grammar.getName())) {
					//System.out.println(progam.getProgramName() + " " + grammar.getName());
					parallelUsage.put(progam.getProgramName(), parallelUsage.get(progam.getProgramName()) + 1);
				}
			}
		}
	}

	public Map<String, Integer> getParallelUsage() {
		return parallelUsage;
	}

	public void setParallelUsage(Map<String, Integer> parallelUsage) {
		this.parallelUsage = parallelUsage;
	}

	public double getMean() {
		return mean;
	}

	public void setMean(double mean) {
		this.mean = mean;
	}

	public double getMedian() {
		return median;
	}

	public void setMedian(double median) {
		this.median = median;
	}

	public double getMode() {
		return mode;
	}

	public void setMode(double mode) {
		this.mode = mode;
	}
	
	public Map<String, Integer> getLanguageCore() {
		return languageCore;
	}

	public void setLanguageCore(Map<String, Integer> languageCore) {
		this.languageCore = languageCore;
	}

	public String toString() {

		return "Mean: " + mean + " Median: " + median + " Mode :" + mode + "\n\n" + parallelUsage;
	}
}
