package com.juliaparse.parser.projectBuilder;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import com.juliaparse.gitget.Config;

public class Formatter {

	public void printLOC(ArrayList<Program> programList) throws IOException {

		File file = new File(Configurations.PATH_TO_HTML_OUTPUT_LOC);
		if (!file.exists()) {
			file.createNewFile();
		}
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter writeHtml = new BufferedWriter(fw);

		BufferedReader readerHeader = new BufferedReader(new FileReader(Configurations.PATH_TO_TEMPLATES + "bootstrap_header_template.html"));
		BufferedReader readerFooter = new BufferedReader(new FileReader(Configurations.PATH_TO_TEMPLATES + "bootstrap_footer_template.html"));

		String header;
		while ((header = readerHeader.readLine()) != null) {
			writeHtml.write(header + "\n");
		}

		writeHtml.write("		<h1>Usage Analysis of the Julia Programming Language</h1>\n");
		writeHtml.write("		<h3>Total Lines of Julia Code Analyzed: " + programList.get(0).getTotalLOC() + "</h3>" + "\n");
		writeHtml.write("		<table class=\"table\">" + "\n");
		writeHtml.write("			<tr>" + "\n");
		writeHtml.write("				<th>Program Name</th>" + "\n");
		writeHtml.write("				<th>Lines of Code</th>" + "\n");
		writeHtml.write("			</tr>" + "\n");
		writeHtml.write("			<tbody>" + "\n");

		// System.out.println("Printing File to HTML...");
		for (Program programItem : programList) {
			writeHtml.write("			<tr>" + "\n");
			// System.out.println(grammarItem + ": " + grammarItem.getUsed());
			writeHtml.write("				<td>" + "<a href=" + programItem + "/" + programItem + ".html>" + programItem + "</td>" + "\n");
			writeHtml.write("				<td>" + programItem.getLOC() + "</td>" + "\n");
			writeHtml.write("			</tr>" + "\n");
		}

		writeHtml.write("			</tbody>" + "\n");
		writeHtml.write("		</table>" + "\n");

		String footer;
		while ((footer = readerFooter.readLine()) != null) {
			writeHtml.write(footer + "\n");
		}

		readerHeader.close();
		readerFooter.close();
		writeHtml.close();

	}

	public void print(Program program) throws IOException {
		ArrayList<Grammar> grammarList = program.getGrammar();
		File folder = new File(Configurations.PATH_TO_HTML_OUTPUT + program.getProgramName());
		if (!folder.exists()) {
			folder.mkdirs();
		}

		File file = new File(Configurations.PATH_TO_HTML_OUTPUT + program.getProgramName() + "\\" + program.getProgramName() + ".html");
		if (!file.exists()) {
			file.createNewFile();
		}
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter writeHtml = new BufferedWriter(fw);

		BufferedReader readerHeader = new BufferedReader(new FileReader(Configurations.PATH_TO_TEMPLATES + "bootstrap_header_template.html"));
		BufferedReader readerFooter = new BufferedReader(new FileReader(Configurations.PATH_TO_TEMPLATES + "bootstrap_footer_template.html"));

		String header;
		while ((header = readerHeader.readLine()) != null) {
			writeHtml.write(header + "\n");
		}
		writeHtml.write("		<h1>Julia Program: " + program.getProgramName() + "</h1>" + "\n");
		writeHtml.write("		<table class=\"table\">" + "\n");
		writeHtml.write("			<tr>" + "\n");
		writeHtml.write("				<th>Name</th>" + "\n");
		writeHtml.write("				<th>Usage</th>" + "\n");
		writeHtml.write("				<th>Category</th>" + "\n");
		writeHtml.write("			</tr>" + "\n");
		writeHtml.write("			<tbody>" + "\n");

		// System.out.println("Printing File to HTML...");
		for (Grammar grammarItem : grammarList) {
			if (grammarItem.getUsed() > 0) {
				writeHtml.write("			<tr>" + "\n");
				// System.out.println(grammarItem + ": " + grammarItem.getUsed());
				writeHtml.write("				<td>" + grammarItem + "</td>" + "\n");
				writeHtml.write("				<td>" + grammarItem.getUsed() + "</td>" + "\n");
				writeHtml.write("<td>" + grammarItem.getCategory() + "</td>" + "\n");
				writeHtml.write("			</tr>" + "\n");
			}
		}

		writeHtml.write("			</tbody>" + "\n");
		writeHtml.write("		</table>" + "\n");

		String footer;
		while ((footer = readerFooter.readLine()) != null) {
			writeHtml.write(footer + "\n");
		}

		readerHeader.close();
		readerFooter.close();
		writeHtml.close();

	}

	public void printJuliaProgramSize(GQM gqm) throws IOException {

		File folder = new File(Configurations.PATH_TO_HTML_OUTPUT + "gqm");
		if (!folder.exists()) {
			folder.mkdirs();
		}

		File file = new File(Configurations.PATH_TO_HTML_OUTPUT + "gqm\\program_size.html");
		if (!file.exists()) {
			file.createNewFile();
		}
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter writeHtml = new BufferedWriter(fw);

		BufferedReader readerHeader = new BufferedReader(new FileReader(Configurations.PATH_TO_TEMPLATES + "bootstrap_header_template.html"));
		BufferedReader readerFooter = new BufferedReader(new FileReader(Configurations.PATH_TO_TEMPLATES + "bootstrap_footer_template.html"));

		String header;
		while ((header = readerHeader.readLine()) != null) {
			writeHtml.write(header + "\n");
		}
		writeHtml.write("		<h1>How large are Julia programs?</h1>" + "\n");
		writeHtml.write("		<table class=\"table\">" + "\n");
		writeHtml.write("			<tr>" + "\n");
		writeHtml.write("				<th>Measurment</th>" + "\n");
		writeHtml.write("				<th>Size</th>" + "\n");
		writeHtml.write("			</tr>" + "\n");
		writeHtml.write("			<tbody>" + "\n");

		writeHtml.write("			<tr>" + "\n");
		writeHtml.write("				<td>Mean</td>\n");
		writeHtml.write("				<td>" + gqm.getMean() + "</td>" + "\n");
		writeHtml.write("			</tr>" + "\n");

		writeHtml.write("			<tr>" + "\n");
		writeHtml.write("				<td>Median</td>\n");
		writeHtml.write("				<td>" + gqm.getMedian() + "</td>" + "\n");
		writeHtml.write("			</tr>" + "\n");

		writeHtml.write("			<tr>" + "\n");
		writeHtml.write("				<td>Mode</td>\n");
		writeHtml.write("				<td>" + gqm.getMode() + "</td>" + "\n");
		writeHtml.write("			</tr>" + "\n");

		writeHtml.write("			</tbody>" + "\n");
		writeHtml.write("		</table>" + "\n");

		String footer;
		while ((footer = readerFooter.readLine()) != null) {
			writeHtml.write(footer + "\n");
		}

		readerHeader.close();
		readerFooter.close();
		writeHtml.close();

	}

	public void printSynchronizedUsage(GQM gqm) throws IOException {
		Map<String, Integer> parallelUsage = new HashMap<String, Integer>();
		parallelUsage.putAll(gqm.getParallelUsage());

		File folder = new File(Configurations.PATH_TO_HTML_OUTPUT + "gqm");
		if (!folder.exists()) {
			folder.mkdirs();
		}

		File file = new File(Configurations.PATH_TO_HTML_OUTPUT + "gqm\\parallel_usage.html");
		if (!file.exists()) {
			file.createNewFile();
		}
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter writeHtml = new BufferedWriter(fw);

		BufferedReader readerHeader = new BufferedReader(new FileReader(Configurations.PATH_TO_TEMPLATES + "bootstrap_header_template.html"));
		BufferedReader readerFooter = new BufferedReader(new FileReader(Configurations.PATH_TO_TEMPLATES + "bootstrap_footer_template.html"));

		String header;
		while ((header = readerHeader.readLine()) != null) {
			writeHtml.write(header + "\n");
		}
		writeHtml.write("		<h1>How often are the parallel features of the language used?</h1>\n");
		writeHtml.write("		<table class=\"table\">" + "\n");
		writeHtml.write("			<tr>" + "\n");
		writeHtml.write("				<th>Name</th>" + "\n");
		writeHtml.write("				<th>Parallel Usage</th>" + "\n");
		writeHtml.write("			</tr>" + "\n");
		writeHtml.write("			<tbody>" + "\n");

		// System.out.println("Printing File to HTML...");
		for (Map.Entry<String, Integer> entry : parallelUsage.entrySet()) {
			writeHtml.write("			<tr>" + "\n");
			// System.out.println(grammarItem + ": " + grammarItem.getUsed());
			writeHtml.write("				<td>" + entry.getKey() + "</td>" + "\n");
			writeHtml.write("				<td>" + entry.getValue() + "</td>" + "\n");
			writeHtml.write("			</tr>" + "\n");
		}

		writeHtml.write("			</tbody>" + "\n");
		writeHtml.write("		</table>" + "\n");

		String footer;
		while ((footer = readerFooter.readLine()) != null) {
			writeHtml.write(footer + "\n");
		}

		readerHeader.close();
		readerFooter.close();
		writeHtml.close();

	}

	public void printLanguageCore(GQM gqm) throws IOException {
		Map<String, Integer> languageCore = new HashMap<String, Integer>();
		languageCore = gqm.getLanguageCore();

		File folder = new File(Configurations.PATH_TO_HTML_OUTPUT + "gqm");
		if (!folder.exists()) {
			folder.mkdirs();
		}

		File file = new File(Configurations.PATH_TO_HTML_OUTPUT + "gqm\\language_core.html");
		if (!file.exists()) {
			file.createNewFile();
		}
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter writeHtml = new BufferedWriter(fw);

		BufferedReader readerHeader = new BufferedReader(new FileReader(Configurations.PATH_TO_TEMPLATES + "bootstrap_header_template.html"));
		BufferedReader readerFooter = new BufferedReader(new FileReader(Configurations.PATH_TO_TEMPLATES + "bootstrap_footer_template.html"));

		String header;
		while ((header = readerHeader.readLine()) != null) {
			writeHtml.write(header + "\n");
		}


		writeHtml.write("		<h1>What defines the languages core?</h1>\n");
		writeHtml.write("		<table class=\"table\">" + "\n");
		writeHtml.write("			<tr>" + "\n");
		writeHtml.write("				<th>Grammar Name</th>" + "\n");
		writeHtml.write("				<th>Total Usage</th>" + "\n");
		writeHtml.write("			</tr>" + "\n");
		writeHtml.write("			<tbody>" + "\n");

		languageCore = addCategoriesToResults(languageCore);

		languageCore = addCategoriesToResults(languageCore);
		
		// System.out.println("Printing File to HTML...");
		for (Map.Entry<String, Integer> entry : languageCore.entrySet()) {
			writeHtml.write("			<tr>" + "\n");
			// System.out.println(grammarItem + ": " + grammarItem.getUsed());
			writeHtml.write("				<td>" + entry.getKey() + "</td>" + "\n");
			writeHtml.write("				<td>" + entry.getValue() + "</td>" + "\n");
			writeHtml.write("			</tr>" + "\n");
		}

		writeHtml.write("			</tbody>" + "\n");
		writeHtml.write("		</table>" + "\n");
		
		writeHtml.write("<table>");
		Iterator<String> it = Config.gramCatCount.keySet().iterator();
	    while (it.hasNext()) {
	    	String key = it.next();
	    	writeHtml.write("<tr><td>"+key+"</td>");
	    	writeHtml.write("<td>"+Config.gramCatCount.get(key).toString()+"</td></tr>");
	    }
	    writeHtml.write("</table>");
	    
		String footer;
		while ((footer = readerFooter.readLine()) != null) {
			writeHtml.write(footer + "\n");
		}

		readerHeader.close();
		readerFooter.close();
		writeHtml.close();

	}


	public Map<String, Integer> addCategoriesToResults(Map<String, Integer> m) {

		Iterator<String> it = m.keySet().iterator();
		LinkedHashMap<String, Integer> chm = new LinkedHashMap<String, Integer>();
		while (it.hasNext()) {
			String key = it.next();
			String cat = Config.gramCatLookup.get(key);
			if (cat == null) {
				cat = "";
			}

			chm.put(key + " (" + cat + ")", m.get(key));

			if (Config.gramCatCount.get(cat) == null) {
				Config.gramCatCount.put(cat, m.get(key));
			} else {
				Config.gramCatCount.put(cat, Config.gramCatCount.get(cat) + m.get(key));
			}
		}
		return chm;
	}
}
