package com.juliaparse.parser.projectBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class projectParser {

	public static void main(String[] args) throws Exception {
		// UNZIP_FOLDER
		// findPrograms(new File(Configurations.PATH_TO_JULIA_PROGRAM));
		findPrograms(new File("C:\\JL_ZIPS"));
		// Print the web pages

	}

	// Goes through the source folder that has all the Julia Programs and starts
	// a list of programs
	public static void findPrograms(final File folder) throws Exception {
		ArrayList<Program> programs = new ArrayList<Program>();
		Formatter printer = new Formatter();
		int x = 0;
		for (final File fileEntry : folder.listFiles()) {

			if (fileEntry.isDirectory()) {
				// Create a program and add it to a list
				// Parse the program and get an array of grammar

				// This combines all the code grammar into one instance per grammar item
				ArrayList<Grammar> grammar = new ArrayList<Grammar>();
				grammar.addAll(getTidyGrammar(findProgramFilesToParse(fileEntry)));

				// Create a program
				programs.add(new Program(fileEntry.getName(), findProgramNameAndLOC(fileEntry), grammar));
				programs.get(programs.size() - 1).getGrammar().size();
				System.out.println("Parse: " + x++);

				// System.out.println("Out Loop : " + findProgramFilesToParse(fileEntry).size());
			}
		}

		// for (Program item : programs) {
		// System.out.println(item.getProgramName() + " " + item.getLOC() + " "
		// + item.getTotalLOC());
		// }
		//
		// for (Program program : programs) {
		// System.out.println(program.getGrammar().size());
		// }

		// GQM and write a file of data out.
		GQM gqm = new GQM(programs);
		// Process program data
		gqm.parallelFeatures();
		gqm.juliaProgramsSize();
		gqm.languageCore();

		// Write the gqm to page
		printer.printJuliaProgramSize(gqm);
		printer.printSynchronizedUsage(gqm);
		printer.printLanguageCore(gqm);

		// print a final program
		printer.printLOC(programs);

		// print each individual programs details
		for (Program program : programs)
			printer.print(program);

	}

	public static ArrayList<Grammar> getTidyGrammar(ArrayList<Grammar> grammar) {

		ArrayList<Grammar> cleanGrammars = new ArrayList<Grammar>();
		ArrayList<String> grammarName = new ArrayList<String>();

		// Create a list of the grammar used
		for (Grammar grammarItem : grammar) {
			if (!grammarName.contains(grammarItem.getName()))
				grammarName.add(grammarItem.getName());
		}

		// For each grammar item, add a grammar and add the used
		int x = 0;
		for (String individualGrammarName : grammarName) {

			cleanGrammars.add(new Grammar(individualGrammarName));
			for (Grammar grammarItem : grammar) {
				if (grammarItem.getName().compareTo(individualGrammarName) == 0) {
					cleanGrammars.get(x).setCategory(grammarItem.getCategory());
					cleanGrammars.get(x).setUsed(cleanGrammars.get(x).getUsed() + grammarItem.getUsed());
					;
				}
			}
			x++;
		}

		return cleanGrammars;
	}

	// ============================================ LOC Methods ===========================================

	// Goes through all the files of an program and counts the lines of code
	public static Integer findProgramNameAndLOC(final File folder) throws Exception {
		Integer count = 0;

		for (final File fileEntry : folder.listFiles()) {
			String path = folder.getAbsolutePath() + "\\" + fileEntry.getName();

			if (fileEntry.isDirectory()) {
				count += findProgramNameAndLOC(fileEntry);
			} else {

				if (fileEntry.getName().length() > 3) {
					// Get the file extension
					String extension = fileEntry.getName().substring(fileEntry.getName().length() - 3, fileEntry.getName().length());

					if (extension.compareTo(".jl") == 0)
						count += parseLinesOfCode(path);
				}
			}
		}
		return count;
	}

	public static Integer parseLinesOfCode(String file) throws IOException {

		Integer LOC = 0;
		String value;
		BufferedReader reader = new BufferedReader(new FileReader(file));
		while ((value = reader.readLine()) != null) {
			// System.out.println(value);
			// System.out.println(!value.isEmpty());
			if (!value.isEmpty()) {
				LOC = LOC + 1;
			}
		}
		// reader.close();
		return LOC;
	}

	// ============================================ Parse Files Methods ===========================================

	public static ArrayList<Grammar> findProgramFilesToParse(final File folder) throws Exception {

		ArrayList<Grammar> usage = new ArrayList<Grammar>();

		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				// If the is a Directory call self
				usage.addAll(findProgramFilesToParse(fileEntry));
			} else {

				if (fileEntry.getName().length() > 3) {
					// Get the file extension
					String extension = fileEntry.getName().substring(fileEntry.getName().length() - 3, fileEntry.getName().length());

					// Parse if the file is a Julia source file
					if (extension.compareTo(".jl") == 0) {
						// String currentProgram;
						// Strictly for readable output at the moment
						// System.out.println("Parsing File:" +
						// folder.getAbsolutePath() + "\\" +
						// fileEntry.getName());
						Parser parser = new Parser();
						usage.addAll(parser.parse(folder.getAbsolutePath() + "\\" + fileEntry.getName()));
					}
				}
			}
		}
		// for (Grammar grammar : usage)
		// if (grammar.getUsed() > 0)
		// System.out.print(grammar + " : ");
		// System.out.println("Done");
		// if (usage.isEmpty())
		// System.out.println("1" + usage);

		// System.out.print(" " + usage.isEmpty());
		// if (usage.isEmpty()) {
		// usage.add(new Grammar("Empty"));
		// System.out.print(" " + usage.isEmpty());
		// }
		// System.out.println("In Loop : " + usage.size());
		return usage;
	}

	// TODO write some test cases, see if combining the grammar cases is the right thing to do
	// This method takes a list of grammar used and adds the instances of a same grammar usage together so there is only one.
	public ArrayList<Grammar> getCombinedGrammar(ArrayList<Grammar> grammar) throws IOException {
		ArrayList<Grammar> combinedGrammar = new ArrayList<Grammar>();

		// Goes through each grammar item and adds the items together
		for (Grammar grammarItem : grammar) {
			combinedGrammar.add(grammarItem);
		}
//Added Change
		return combinedGrammar;
	}

}
