package com.juliaparse.parser.projectBuilder;

public class Grammar {

	private String category;
	private String subCategory;
	private String name;
	private Integer used;
	private String programName;

	public Grammar(String category, String subCategory, String name) {
		this.category = category;
		this.subCategory = subCategory;
		this.name = name;
		this.used = 0;
	}

	public Grammar(String category, String name) {
		this.category = category;
		this.subCategory = "NO_SUB_CATEGORY";
		this.name = name;
		this.used = 0;
	}
	
	public Grammar(String name) {
		this.category = "NOT_SET";
		this.subCategory = "NO_SUB_CATEGORY";
		this.name = name;
		this.used = 0;
	}

	public Grammar() {
		this.category = "NOT_SET";
		this.subCategory = "NO_SUB_CATEGORY";
		this.name = "NOT_SET";
		this.used = 0;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getUsed() {
		return used;
	}

	public void setUsed(Integer used) {
		this.used = used;
	}
	
	public String toString(){
		return name;
	}

	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}

}
