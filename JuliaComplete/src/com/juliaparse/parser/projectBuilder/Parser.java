package com.juliaparse.parser.projectBuilder;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.simple.parser.ParseException;

public class Parser {
	ArrayList<String> usage = new ArrayList<String>();

	public ArrayList<Grammar> parse(String file) throws IOException, ParseException {
		// Pass the file location as an argument
		BufferedReader reader = new BufferedReader(new FileReader(file));
		// Turn into grammar items
		// Load the grammar data from a json file
		ArrayList<Grammar> grammar = Data.getInstance().loadData();

		int value = 0;
		StringBuffer temp = new StringBuffer();
		// reads to the end of the stream
		while ((value = reader.read()) != -1) {

			char c = (char) value;
			temp.append(c);
			String tempString = temp.toString();

			// Determines if it's a function call
			if (tempString.matches("\\s*\\S+[(]")) {
				tempString += getFunction(value, reader);
				tempString = tempString.replaceAll("\\s", "");
				usage.add(tempString);
				// System.out.println("Function: " + tempString);
				temp.delete(0, temp.length());
				// Checks that it's a reserved word
			} else if (tempString.matches("\\s*\\S+\\s")) {
				tempString = tempString.replaceAll("\\s", "");
				usage.add(tempString);
				// System.out.println("Reserved: " + tempString);
				temp.delete(0, temp.length());
			}

		}
		// reader.close();
		// System.out.println(usage.size());
		return cleanGrammar(grammar);
	}

	// Recursively parse through functions
	public String getFunction(int value, BufferedReader reader) throws IOException {
		StringBuffer temp = new StringBuffer();

		while ((value = reader.read()) != -1) {
			char c = (char) value;
			temp.append(c);

			if (c == ')') {
			
				return temp.toString();
			} else if (c == '(') {
				temp.append(getFunction(value, reader));
				usage.add(temp.toString());
				// System.out.println("Function: " + temp);
				temp.delete(0, temp.length());
			}

		}
		// reader.close();
		return "SHOULDN'T HAPPEN";
	}

	public ArrayList<Grammar> cleanGrammar(ArrayList<Grammar> grammar) throws IOException {
		// System.out.println(usage);
		// Compare usage for every item
		// System.out.println(usage);
		for (String usageItem : usage) {
			for (Grammar grammarItem : grammar) {
				// This is where the bug is, I need it to add some regex here so that printLn doens't accept in, print, int, ..etc //TODO
				if (usageItem.contains(grammarItem.toString())) {
					grammarItem.setUsed(grammarItem.getUsed() + 1);
				}

			}
		}
		
		// clean error from contains method
		ArrayList<Grammar> compareGrammar = new ArrayList<Grammar>();
		compareGrammar.addAll(grammar);
		for (Grammar finalListGrammarItem : grammar) {
			int grammarItemCount = 0;
			for (Grammar grammarToCompare : compareGrammar) {

				if (grammarToCompare.getName().contains(finalListGrammarItem.getName()) && grammarToCompare.getName().compareTo(finalListGrammarItem.getName()) != 0) {
					grammarItemCount += grammarToCompare.getUsed();
				}

			}
			finalListGrammarItem.setUsed(finalListGrammarItem.getUsed() - grammarItemCount);
		}

		// Creates a list of used grammar
		ArrayList<Grammar> finalList = new ArrayList<Grammar>();
		for (Grammar grammarItem : grammar) {
			if (grammarItem.getUsed() > 0) {
				finalList.add(grammarItem);
			}
		}

		return finalList;
	}

}
