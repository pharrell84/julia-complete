package com.juliaparse.parser.projectBuilder;

public class Configurations {

	public static final String PATH_TO_JSON_FILE = "JSON\\grammar.json";
	public static final String PATH_TO_JULIA_PROGRAM = "TEST_PROGRAM";
	public static final String PATH_TO_HTML_OUTPUT = "HTML_OUTPUT\\";
	public static final String PATH_TO_HTML_OUTPUT_LOC = "HTML_OUTPUT\\index.html";
	public static final String PATH_TO_TEMPLATES = "TEMPLATE\\";

}
