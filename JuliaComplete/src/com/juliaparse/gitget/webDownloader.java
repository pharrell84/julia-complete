package com.juliaparse.gitget;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class webDownloader {
	

        @SuppressWarnings("static-access")
        public static void main(String[] args) throws InterruptedException, IOException {
                String lastElement;
                String currentElement;
                Elements pages;
                String nextPage = "";
                Document doc;

                // Pull all the HTML from the Julia Document
                doc = Jsoup.connect(Config.PATH_TO_GITHUB).userAgent("Mozilla").get();

                // Get the last page in the pagenation
                Elements lastPage = doc.select("div.main-content > div.pagination > a:not(.next_page)");
                lastElement = lastPage.last().text();
                System.out.println("Last Page:" + lastElement);
                
                // Find the current page
                Elements current = doc.select("div.main-content > div.pagination > span.current");
                currentElement = current.text();

                // Navigate through all the pages and list names
                while (currentElement.compareTo(lastElement) != 0) {
                        System.out.println("Current Page: " + current.text());
                        System.out.println("Current Element " + currentElement + " vs " + lastElement + " Result: "
                                        + currentElement.compareTo(lastElement));
                        System.out.println();
                        
                        pullProgInfo(doc);
                        
                     // Find the link to the next page
                        pages = doc.select("div.main-content > div.pagination > a");
                        nextPage = pages.last().attr("href");
                        System.out.println("Next Page: " + nextPage);
                        
                        Thread.sleep(Config.PAGE_DELAY);
                        boolean success = false;
                        while (!success) {
	                        try {
	                        	doc = Jsoup.connect(Config.GITHUB + nextPage).userAgent("Mozilla").get();
	                            current = doc.select("div.main-content > div.pagination > span.current");
	                            currentElement = current.text();
	                            success = true;
	                        }
	                        catch (HttpStatusException e) {
	                        	if (e.getStatusCode() == 420) {
	                        		System.out.println("420 error, rate limit -- delay next pull for "+ String.valueOf((Config.RATE_LIMIT_DELAY / 1000)) + " seconds.");
	                        		Thread.sleep(Config.RATE_LIMIT_DELAY);
	                        	} else {
	                        		System.out.println("Error loading page: " + nextPage + ", exiting the system");
	                                e.printStackTrace();
	                                System.exit(0);
	                        	}
	                        } catch (IOException e) {
	                                System.out.println("Error loading page: " + nextPage + ", exiting the system");
	                                e.printStackTrace();
	                                System.exit(0);
	                        }
                        }
                }
        }

        static ArrayList<String> subPages = new ArrayList<String>();
        static ArrayList<String> archiveLocs = new ArrayList<String>();
        
        public static void pullProgInfo(Document doc) {
        	Elements sections = doc.select("div.main-content > ul.repolist > li.public > h3.repolist-name > a");
            for (Element section : sections) {
                    System.out.println("Julia Program Name: " + section.text());
                    subPages.add(section.text());
                    fillArchiveLocs(section.text(), section.text());
                    try { Thread.sleep(5000); } catch (Exception e) { e.printStackTrace(); }
                    //https://github.com/JuliaStats/Distributions.jl/archive/master.zip - just a reference for ZIP formatting for myself...
                    //https://github.com/JuliaLang/METADATA.jl/archive/metadata-v2.zip
            }
            System.out.println();
        }
        
        public static void fillArchiveLocs(String projLoc, String projName) {
    		try {
    			Document doc = Jsoup.connect(Config.GITHUB + "/"+ projLoc).userAgent("Mozilla").get();
                Elements sections = doc.select("div.only-with-full-nav > a");
                boolean gotZIP = false;
                for (Element section : sections) {
                	if (section.attr("href") != null) {
                    	if (section.attr("href").contains(projLoc)) { //section.getElementsByAttributeValueContaining("href", subPages.get(g))
                    		System.out.println("Archive loc: " + section.attr("href"));
                    		archiveLocs.add(section.attr("href"));
                    		gotZIP = downloadZIP(section.attr("href"), projName);
                    	}
                	}
                }
                if (gotZIP) {
                	Thread.sleep(5000);
                }
                
    		} catch (Exception e) { e.printStackTrace(); }
        }
        
        public static boolean downloadZIP(String zipName, String projName) {
        	try {
        		if (!(new File(Config.DL_FOLDER)).exists()) {
        			(new File(Config.DL_FOLDER)).mkdirs();
        		}
        		String whereToSave = Config.DL_FOLDER+projName+"\\"+Config.LOCAL_ZIP_NAME;
        		if (!(new File(whereToSave)).exists()) {
	        		// master.zip is not right for them all... but for now it will get a lot?
	        		System.out.println("Wait 5 seconds, get ZIP:"+projName);
	        		Thread.sleep(5000);
	        		URL website = new URL(Config.GITHUB+zipName);
	        		ReadableByteChannel rbc = Channels.newChannel(website.openStream());
	        		(new File(Config.DL_FOLDER+projName)).mkdirs();
	        		String zipLoc = whereToSave;
	        		FileOutputStream fos = new FileOutputStream(zipLoc);
	        		fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
	        		fos.close();
	        		//zipFilesFound.add(zipLoc);
	        		return true;
        		} else { System.out.println("ZIP already downloaded, skipping: "+projName); return false; }
        	} catch (Exception e) {
        		e.printStackTrace();
        	}
        	
        	return false;
        	
        	// credit http://stackoverflow.com/questions/921262/how-to-download-and-save-a-file-from-internet-using-java for saving me time
        }
        
        
        
        public static void unzipJLDownloads() {
        	// credit to http://www.mkyong.com/java/how-to-decompress-files-from-a-zip-file/ though it didn't work as is... folder issues existed
        	// not 100% sure this would work if called inside getZipFiles when it populates the array, but it works with populateFileList...
        	// it's a question of if the files are formatted the same in the array, easy fix if it doesnt
        	
        	ArrayList<String> zipFilesFound = findFilesWithSuffix(Config.DL_FOLDER, ".zip");
        	
        	if (zipFilesFound == null) { System.out.println("Are you sure you had files in "+Config.DL_FOLDER+"?  Program gets null files");
				return;
        	}	
    		if (zipFilesFound.size() == 0) { System.out.println("Are you sure you had files in "+Config.DL_FOLDER+"?  Program gets null files");
				return;
    		}	
        	
        	byte[] buffer = new byte[1024];
        	
        	for (int i = 0; i < zipFilesFound.size(); i++) {
        		System.out.println("CHECKING FOR UNZIP: "+zipFilesFound.get(i));
        		try {
        			String unzipLoc = zipFilesFound.get(i).substring(0, zipFilesFound.get(i).lastIndexOf("\\"))+"/unzipped/";
            		(new File(unzipLoc)).mkdirs();
            		System.out.println("Search for zip file in: "+zipFilesFound.get(i));
    				ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFilesFound.get(i)));
    				ZipEntry ze = zis.getNextEntry();
    				while(ze!=null){
			    	   String fileName = ze.getName();
			           File newFile = new File(unzipLoc + fileName);
			 
			           //System.out.println("file unzip : "+ newFile.getAbsoluteFile() + " and is dir?" + String.valueOf(ze.isDirectory()));
			 
			            //create all non exists folders
			            //else you will hit FileNotFoundException for compressed folder
			            new File(newFile.getParent()).mkdirs();
			            
			            if (ze.isDirectory()) {
			            	newFile.mkdirs();
			            } else {
				            FileOutputStream fos = new FileOutputStream(newFile);             
				 
				            int len;
				            while ((len = zis.read(buffer)) > 0) {
				            	fos.write(buffer, 0, len);
				            }
				 
				            fos.close();
			            }
			            ze = zis.getNextEntry();
			    	}
			 
			        zis.closeEntry();
			    	zis.close();
			 
			    	System.out.println("Done unzipping "+zipFilesFound.get(i));
    			} catch (FileNotFoundException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			} catch (Exception ex) {
    				ex.printStackTrace();
    			}
        	}
        }
        
        public static String findFile(String root, String zipName) {
    		File dir = null;
    		if (root ==  null) { return null; }  else { dir = new File(root); }
        	File[] files = dir.listFiles();
    		for (int b = 0; b < files.length; b++) {
    			if (files[b].isDirectory()) {
    				String foundIt = findFile(dir.getAbsolutePath()+"/"+files[b].getName(), zipName);
    				if (foundIt != null) { return foundIt; }
    			}
    			//System.out.println(files[b].getName());
        		if (files[b].getName().toString().equalsIgnoreCase(zipName)) {
        			//System.out.println("FOUND "+files[b].getAbsolutePath());
        			return files[b].getAbsolutePath();
        		}
        	}
    		
    		return null;
    	}
        
        public static ArrayList<String> findFilesWithSuffix(String root, String suffix) {
        	return findFilesWithSuffix(root, suffix, null);
        }
        
        //static ArrayList<String> zipFilesFound = new ArrayList<String>();
        private static ArrayList<String> findFilesWithSuffix(String root, String suffix, ArrayList<String> inAr) {
        	if (inAr == null) { inAr = new ArrayList<String>(); }
    		File dir = null;
    		if (root ==  null) { return null; }  else { dir = new File(root); }
        	File[] files = dir.listFiles();
    		for (int b = 0; b < files.length; b++) {
    			if (files[b].isDirectory()) {
    				inAr = findFilesWithSuffix(dir.getAbsolutePath()+"/"+files[b].getName(), suffix, inAr);
    				continue;
    			}
    			//System.out.println(files[b].getName());
    			if (files[b].getName().lastIndexOf(".") > -1) {
	        		if (files[b].getName().substring(files[b].getName().lastIndexOf(".")).equalsIgnoreCase(suffix)) {
	        			//System.out.println("FOUND A "+suffix+": "+files[b].getAbsolutePath());
	        			inAr.add(files[b].getAbsolutePath());
	        		}
    			}
        	}
    		
    		return inAr;
    	}

}