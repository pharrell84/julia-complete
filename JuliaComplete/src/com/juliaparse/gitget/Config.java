package com.juliaparse.gitget;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.juliaparse.parser.projectBuilder.Grammar;

public class Config {
        // This is the url string to get a list of all the Julia projects with one star
        static final String PATH_TO_GITHUB = "https://github.com/search?l=julia&q=stars%3A%3E1&type=Repositories";
        // GitHub's base path
        static final String GITHUB = "https://github.com";
        public static final String DL_FOLDER = "C:\\JL_ZIPS\\";
        public static final String UNZIP_FOLDER = "C:\\JL_UNZIP\\";
        public static final String LOCAL_ZIP_NAME = "master.zip"; // this will need to be dynamic at some point, some projects dont use this... dont know how many though
        
        static final int PAGE_DELAY = 10000;
        static final int RATE_LIMIT_DELAY = 60000;
        public static ArrayList<String> isRunning = new ArrayList<String>();
        public static List<Grammar> grammar = null;
        public static ConcurrentHashMap<String, ArrayList<Grammar>> ghm = new ConcurrentHashMap<String, ArrayList<Grammar>>();
        
        public static ConcurrentHashMap<String, String> gramCatLookup = new ConcurrentHashMap<String, String>();
        public static ConcurrentHashMap<String, Integer> gramCatCount = new ConcurrentHashMap<String, Integer>();
}