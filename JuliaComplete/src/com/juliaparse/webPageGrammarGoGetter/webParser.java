package com.juliaparse.webPageGrammarGoGetter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.juliaparse.parser.projectBuilder.Grammar;

public class webParser {

	/**
	 * This goes to the Julia document page and parses the grammar.
	 * 
	 * @throws IOException
	 */

	public static void main(String[] args) throws IOException {

		ArrayList<Grammar> grammarList = new ArrayList<Grammar>();
		ArrayList<Grammar> secondGrammarList = new ArrayList<Grammar>();

		// Pull all the HTML from the Julia Document
		Document doc = Jsoup.connect(WepPageParserConfig.PATH_TO_PARSE_JULIA_LANGUAGE).userAgent("Mozilla").get();

		// ======= Get all the grammar from the integer sub category ======
		Elements sections = doc.select("div#numbers > div.section");
		// System.out.println("\nnumbers");
		for (Element section : sections) {
			// System.out.println("\n" + "SubSection : " + section.id());

			Elements names = section.getElementsByTag("tt");
			for (Element name : names) {

				if (name.ownText().compareTo("") != 0) {
					// System.out.println("Section : " + name.ownText());
					grammarList.add(new Grammar("numbers", section.id(), name.ownText()));
				}
			}

		}

		// ======== Get all the grammar from the Arrays sub category ======
		sections.clear();
		sections = doc.select("div#arrays > div.section");
		// System.out.println("\narrays");
		for (Element section : sections) {
			// System.out.println("\n" + "SubSection : " + section.id());

			Elements names = section.getElementsByTag("tt");
			for (Element name : names) {

				if (name.ownText().compareTo("") != 0) {
					// System.out.println("Section : " + name.ownText());
					grammarList.add(new Grammar("arrays", section.id(), name.ownText()));
				}
			}

		}

		// =========== Get everything else ========
		sections.clear();
		sections = doc.select("div.section:not(#arrays):not(#numbers)");
		for (Element section : sections) {
			// System.out.println("\n" + "Section : " + section.id());

			Elements names = section.getElementsByTag("tt");
			for (Element name : names) {

				if (name.ownText().compareTo("") != 0) {
					// System.out.println("Section : " + name.ownText());
					grammarList.add(new Grammar(section.id(), name.ownText()));
				}
			}
		}

		for (Grammar item : grammarList) {
			secondGrammarList.add(item);
		}
		
		System.out.println(grammarList.size());
		System.out.println(secondGrammarList.size());

		for (Grammar item : grammarList) {
			for (Grammar nextItem : secondGrammarList) {

				if (item.getName().compareTo(nextItem.getName()) == 0)
					secondGrammarList.remove(nextItem);
			}

		}

		//System.out.println(grammarList);
		// System.out.println(grammarList.size());
		// writeJSON(grammarList);

	}

	@SuppressWarnings("unchecked")
	public static void writeJSON(ArrayList<Grammar> grammarList) throws IOException {

		// Path to write the json file to
		File f = new File(WepPageParserConfig.PATH_TO_WRITE_JSON_FILE);
		if (!f.exists()) {
			f.createNewFile();
		}
		FileWriter file = new FileWriter(f.getAbsoluteFile());
		ArrayList<Grammar> grammars = grammarList;
		JSONArray list = new JSONArray();
		JSONObject object = new JSONObject();

		// Take the grammar list and create a json object
		for (Grammar item : grammars) {
			JSONObject obj = new JSONObject();
			obj.put("category", item.getCategory());
			obj.put("subCategory", item.getSubCategory());
			obj.put("name", item.getName());
			obj.put("used", 0);
			list.add(obj);
		}
		object.put("grammar", list);
		// System.out.print(object);

		// Write the file
		file.write(object.toJSONString());
		file.flush();
		file.close();
	}
}
