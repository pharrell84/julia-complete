package main;

import java.io.IOException;

import com.juliaparse.gitget.CategoryDownloader;
import com.juliaparse.gitget.webDownloader;
import com.juliaparse.parser.projectBuilder.projectParser;


public class StartUp {
	
	private static final boolean RUN_GIT_PULL = false; //this can take a while and doesnt update often
	private static final boolean RUN_UNZIP = false;
	private static final boolean RUN_PARSE = true;
	
	
	
	public static void main(String[] args) {
		
		
		
		//webDownloader.findFilesWithSuffix(Config.DL_FOLDER, ".zip");
		
		if (RUN_GIT_PULL) {
			try {
				System.out.println("RUNNING GIT GET");
				webDownloader.main(null);				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if (RUN_UNZIP) {
			webDownloader.unzipJLDownloads();
		}
		
		if (RUN_PARSE) {
			try {
				CategoryDownloader.main(null); // pulls categories from the julia page for each function, to add to output for analysis, can run w/o it
				
				projectParser.main(null);
			} catch (Exception e) {
				
			}
			
		}
		
	}
}
